# coding=utf-8
# Copyright 2021 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Entry point for AWD ENAS search process."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from npu_bridge.npu_init import *
from tensorflow.core.protobuf.rewriter_config_pb2 import RewriterConfig

import os
import pickle
import sys
import time

sys.path.append("/home/ma-user/modelarts/user-job-dir/")

import numpy as np
import tensorflow.compat.v1 as tf

from enas_lm_npu_for_TensorFlow.enas_lm_npu_20211114162907.src import child
from enas_lm_npu_for_TensorFlow.enas_lm_npu_20211114162907.src import controller
from enas_lm_npu_for_TensorFlow.enas_lm_npu_20211114162907.src import utils
from tensorflow.contrib import training as contrib_training


flags = tf.app.flags
gfile = tf.gfile
FLAGS = flags.FLAGS

## Required parameters
flags.DEFINE_string('output_dir', None, '')
flags.DEFINE_string('data_path', None, '')
flags.DEFINE_string("obs_dir", "obs://rstg/log", "obs result path, not need on gpu and apulis platform")


## Other parametersresult
flags.DEFINE_boolean('reset_output_dir', False, '')
flags.DEFINE_string("platform", "apulis", "Run on apulis/modelarts platform. Modelarts Platform has some extra data copy operations")

flags.DEFINE_integer('log_every', 20, '')



def get_ops(params, x_train, x_valid):
    """Build [train, valid, test] graphs."""

    ct = controller.Controller(params=params)
    lm = child.LM(params, ct, x_train, x_valid)
    ct.build_trainer(lm)
    params.add_hparam('num_train_batches', lm.num_train_batches)
    ops = {
        'train_op': lm.train_op,
        'learning_rate': lm.learning_rate,
        'grad_norm': lm.grad_norm,
        'train_loss': lm.train_loss,
        'l2_reg_loss': lm.l2_reg_loss,
        'global_step': tf.train.get_or_create_global_step(),
        'reset_batch_states': lm.batch_init_states['reset'],
        'eval_valid': lm.eval_valid,

        'reset_start_idx': lm.reset_start_idx,
        'should_reset': lm.should_reset,
        'bptt_rate': lm.bptt_rate,

        'controller_train_op': ct.train_op,
        'controller_grad_norm': ct.train_op,
        'controller_sample_arc': ct.sample_arc,
        'controller_entropy': ct.sample_entropy,
        'controller_reward': ct.reward,
        'controller_baseline': ct.baseline,
        'controller_optimizer': ct.optimizer,
        'controller_train_fn': ct.train,

    }
    print('-' * 80)
    print('HParams:\n{0}'.format(params.to_json(indent=2, sort_keys=True)))

    return ops

def load_ckpt_model(sess, save_path):
    print("reload model from:{}".format(save_path))
    checkpoint = tf.train.get_checkpoint_state(save_path)  # 从checkpoint文件中读取checkpoint对象
    input_checkpoint = checkpoint.model_checkpoint_path
    saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=True)  # 加载模型结构
    saver.restore(sess, input_checkpoint)  # 使用最新模型
    sess.run(tf.global_variables_initializer())# 初始化所有变量

def train(params):
    """Entry train function."""
    print("data_path:{}".format(params.data_path))
    print("output_dir:{}".format(params.output_dir))
    with gfile.GFile(params.data_path, 'rb') as finp:
        x_train, x_valid, _, _, _ = pickle.load(finp)
        print('-' * 80)
        print('train_size: {0}'.format(np.size(x_train)))
        print('valid_size: {0}'.format(np.size(x_valid)))


    g = tf.Graph()
    with g.as_default():
        tf.random.set_random_seed(2126)
        ops = get_ops(params, x_train, x_valid)
        run_ops = [
            ops['train_loss'],
            ops['l2_reg_loss'],
            ops['grad_norm'],
            ops['learning_rate'],
            ops['should_reset'],
            ops['train_op'],
        ]

        saver = tf.train.Saver(max_to_keep=5)
        checkpoint_saver_hook = tf.train.CheckpointSaverHook(
            params.output_dir, save_steps=params.num_train_batches, saver=saver)
        hooks = [checkpoint_saver_hook]
        hooks.append(ops['controller_optimizer'].make_session_run_hook(True))

        # >>> add code >>
        # 创建session
        config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        custom_op = config.graph_options.rewrite_options.custom_optimizers.add()
        custom_op.name = "NpuOptimizer"
        custom_op.parameter_map["use_off_line"].b = True  # 在昇腾AI处理器执行训练
        custom_op.parameter_map["mix_compile_mode"].b = False  # 关闭混合计算，根据实际情况配置，默认关闭
        # custom_op.parameter_map["precision_mode"].s = tf.compat.as_bytes("allow_mix_precision") # 设置混合精度
        custom_op.parameter_map["fusion_switch_file"].s = tf.compat.as_bytes("/home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/fusion_switch.cfg")
        # custom_op.parameter_map["dump_path"].s = tf.compat.as_bytes("/home/ma-user/modelarts/inputs/data_url_0")
        #
        # custom_op.parameter_map["enable_dump_debug"].b = True
        # custom_op.parameter_map["dump_debug_mode"].s = tf.compat.as_bytes("all")
        # # custom_op.parameter_map["enable_data_pre_proc"].b = True  # getnext算子下沉是迭代循环下沉的必要条件
        # # custom_op.parameter_map[
        # #     "iterations_per_loop"].i = 10  # 此处设置的值和set_iteration_per_loop设置的iterations_per_loop值保持一致，用于判断是否进行训练迭代下沉
        #
        config.graph_options.rewrite_options.remapping = RewriterConfig.OFF  # 必须显式关闭
        config.graph_options.rewrite_options.memory_optimization = RewriterConfig.OFF  # 必须显式关闭
        # sess = tf.train.SingularMonitoredSession(config=config, hooks=hooks, checkpoint_dir=params.output_dir)
        # >>> add code >>

        sess = tf.train.SingularMonitoredSession(config=config, hooks=hooks,
                                                 checkpoint_dir=params.output_dir)
        # reload model
        if len(gfile.ListDirectory(params.output_dir)):
            last_checkpoint = tf.train.latest_checkpoint(params.output_dir)
            print('rolling back to previous checkpoint {0}'.format(last_checkpoint))
            saver.restore(sess, last_checkpoint)

        accum_loss = 0
        accum_step = 0
        epoch = sess.run(ops['global_step']) // params.num_train_batches
        best_valid_ppl = []
        start_time = time.time()
        last_mins = (time.time() - start_time) / 60
        accum_rate = 0.
        # sess.run(tf.global_variables_initializer())
        while True:
            try:
                # run_ops = [
                # ops['train_loss'],
                # ops['l2_reg_loss'],
                # ops['grad_norm'],
                # ops['learning_rate'],
                # ops['should_reset'],
                # ops['train_op'],
                # ]
                # 修改点
                # loss, l2_reg, gn, lr, should_reset, _ = sess.run(run_ops)
                loss = sess.run(ops['train_loss'])
                # print("loss_OK:loss:{}".format(loss))
                l2_reg = sess.run(ops['l2_reg_loss'])
                # print("l2_reg_OK:l2_reg:{}".format(l2_reg))
                gn = sess.run(ops['grad_norm'])
                # gn = -111111
                # print("gn_OK:gn:{}".format(gn))
                lr = sess.run(ops['learning_rate'])
                # print("lr_OK:le:{}".format(lr))
                should_reset = sess.run(ops['should_reset'])
                _ = sess.run(ops["train_op"])

                bptt_rate = sess.run(ops['bptt_rate'])
                # print("should_reset_OK:should_reset:{}".format(should_reset))
                # if not should_not_train :
                #     _ = sess.run(ops["train_op"])

                accum_loss += loss
                accum_step += 1
                accum_rate += bptt_rate
                step = sess.run(ops['global_step'])
                if step % params.log_every == 0:
                    train_ppl = np.exp(accum_loss / accum_step)
                    mins_so_far = (time.time() - start_time) / 60.
                    mins_pices = mins_so_far - last_mins
                    last_mins = mins_so_far
                    log_string = 'epoch={0:<5d}'.format(epoch)
                    log_string += ' step={0:<7d}/{1:<6d}'.format(step, params.num_train_steps)
                    log_string += ' ppl={0:<9.2f}'.format(train_ppl)
                    log_string += ' lr={0:<7.2f}'.format(lr)
                    log_string += ' |w|={0:<6.2f}'.format(l2_reg)
                    log_string += ' |g|={0:<6.2f}'.format(gn)
                    log_string += ' mins={0:<.2f}-min/step={1:<.4f}'.format(mins_so_far, mins_pices/params.log_every)
                    # log_string += ' accum_rate(rate of a epoch)={0:<4.4f}'.format(accum_rate)
                    # log_string += ' should_reset:{}'.format(should_reset)
                    print(log_string)

                if should_reset:
                    accum_rate=0.
                    print("should_reset:{}".format(should_reset))
                    ops['controller_train_fn'](sess, ops['reset_batch_states'])
                    epoch += 1
                    accum_loss = 0
                    accum_step = 0
                    valid_ppl = ops['eval_valid'](sess)
                    sess.run([ops['reset_batch_states'], ops['reset_start_idx']])
                    best_valid_ppl.append(valid_ppl)

                if step % (params.num_train_batches * 10) == 0:
                    if FLAGS.platform.lower() == 'modelarts':
                        from help_modelarts import modelarts_result2obs
                        modelarts_result2obs(FLAGS)
                if step >= params.num_train_steps:
                    if FLAGS.platform.lower() == 'modelarts':
                        from help_modelarts import modelarts_result2obs
                        modelarts_result2obs(FLAGS)
                    break
            except tf.errors.InvalidArgumentError:
                if FLAGS.platform.lower() == 'modelarts':
                    from help_modelarts import modelarts_result2obs
                    modelarts_result2obs(FLAGS)
                last_checkpoint = tf.train.latest_checkpoint(params.output_dir)
                print('rolling back to previous checkpoint {0}'.format(last_checkpoint))
                saver.restore(sess, last_checkpoint)
        sess.close()


def main(unused_args):

    tf.logging.set_verbosity(tf.logging.INFO)
    tf.logging.info("**********")
    print("===>>>data_path:{}".format(FLAGS.data_path))
    print("===>>>output_dir:{}".format(FLAGS.output_dir))
    print("===>>>obs_dir:{}".format(FLAGS.obs_dir))
    print("===>>>train_step:{}".format(FLAGS.num_train_epochs))

    np.set_printoptions(precision=3, suppress=True, threshold=int(1e9),
                        linewidth=80)

    print('-' * 80)
    if not gfile.IsDirectory(FLAGS.output_dir):
        print('Path {} does not exist. Creating'.format(FLAGS.output_dir))
        gfile.MakeDirs(FLAGS.output_dir)
    elif FLAGS.reset_output_dir:
        print('Path {} exists. Reseting'.format(FLAGS.output_dir))
        gfile.DeleteRecursively(FLAGS.output_dir)
        gfile.MakeDirs(FLAGS.output_dir)

    print('-' * 80)
    log_file = os.path.join(FLAGS.output_dir, 'stdout')
    print('Logging to {}'.format(log_file))
    sys.stdout = utils.Logger(log_file)

    params = contrib_training.HParams(
        data_path=FLAGS.data_path,
        log_every=FLAGS.log_every,
        output_dir=FLAGS.output_dir,
    )
    train(params)



if __name__ == '__main__':
    flags.mark_flag_as_required("data_path")
    flags.mark_flag_as_required("output_dir")
    flags.mark_flag_as_required("obs_dir")
    tf.app.run()
