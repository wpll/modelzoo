# coding=utf-8
# Copyright 2021 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Entry point for AWD ENAS with a fixed architecture."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from npu_bridge.npu_init import *
from tensorflow.python.tools import freeze_graph

import os
import pickle
import sys

# TODO:change path
# sys.path.append("/home/test_user06/AscendZhongzhi_NJU/")
import time

import numpy as np
import tensorflow.compat.v1 as tf

import fixed_lib
import utils
from tensorflow.contrib import training as contrib_training

flags = tf.app.flags
gfile = tf.gfile
FLAGS = flags.FLAGS

## Required parameters
subfolder = str(time.strftime('%Y%m%d_%H%M%S'))
flags.DEFINE_string('output_dir', "./output/infer0/" + subfolder, '')
flags.DEFINE_string('data_path', './ptb/ptb.pkl', '')
flags.DEFINE_string("ckp_path", '', "checkpoint path")

## Other parametersresult
flags.DEFINE_boolean('reload_model', True, '')
flags.DEFINE_boolean('reset_output_dir', True, '')
flags.DEFINE_boolean('is_training', False, '')
flags.DEFINE_string("platform", "apulis", "Run on apulis/modelarts platform. Modelarts Platform has some extra data copy operations")

flags.DEFINE_integer('log_every', 100, '')


def get_ops(params, x_train, x_valid, x_test):
    """Build [train, valid, test] graphs."""
    lm = fixed_lib.LM(params, x_train, x_valid, x_test)
    params.add_hparam('num_train_batches', lm.num_train_batches)
    ops = {
        'train_op': lm.train_op,
        'learning_rate': lm.learning_rate,
        'grad_norm': lm.grad_norm,
        'train_loss': lm.train_loss,
        'global_step': tf.train.get_or_create_global_step(),
        'reset_batch_states': lm.batch_init_states['reset'],
        'eval_valid': lm.eval_valid,
        'eval_test': lm.do_infer,
        'bptt_rate': lm.bptt_rate,

        'reset_start_idx': lm.reset_start_idx,
        'should_reset': lm.should_reset,
        'moving_avg_started': lm.moving_avg_started,
        'update_moving_avg': lm.update_moving_avg_ops,
        'start_moving_avg': lm.start_moving_avg_op,
        'end_moving_avg': lm.end_moving_avg_op,
        'reset_avg': lm.restart_avg,
        'set_lr_decay': lm.set_lr_decay,
        'reset_start_idx_eval': lm.reset_start_idx_eval,
    }
    print('-' * 80)
    print('HParams:\n{0}'.format(params.to_json(indent=2, sort_keys=True)))

    return ops


def load_ckpt_model(sess, save_path):
    print("reload model from:{}".format(save_path))
    checkpoint = tf.train.get_checkpoint_state(save_path)  # 从checkpoint文件中读取checkpoint对象
    input_checkpoint = checkpoint.model_checkpoint_path
    saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=True)  # 加载模型结构
    saver.restore(sess, input_checkpoint)  # 使用最新模型
    sess.run(tf.global_variables_initializer())# 初始化所有变量


def train(params, is_training=True):
    """Entry point for training."""
    with gfile.GFile(params.data_path, 'rb') as finp:
        x_train, x_valid, x_test, _, _ = pickle.load(finp)
        print('-' * 80)
        print('train_size: {0}'.format(np.size(x_train)))
        print('valid_size: {0}'.format(np.size(x_valid)))
        print(' test_size: {0}'.format(np.size(x_test)))

    g = tf.Graph()
    with g.as_default():
        tf.random.set_random_seed(2126)
        ops = get_ops(params, x_train, x_valid, x_test)
        run_ops = [
            ops['train_loss'],
            ops['grad_norm'],
            ops['learning_rate'],
            ops['should_reset'],
            ops['moving_avg_started'],
            ops['train_op'],
        ]

        saver = tf.train.Saver(max_to_keep=2)
        checkpoint_saver_hook = tf.train.CheckpointSaverHook(
            params.output_dir, save_steps=params.num_train_batches, saver=saver)
        hooks = [checkpoint_saver_hook]

        # >>> add code >>
        # 创建session
        config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        custom_op = config.graph_options.rewrite_options.custom_optimizers.add()
        custom_op.name = "NpuOptimizer"
        custom_op.parameter_map["use_off_line"].b = True  # 在昇腾AI处理器执行训练
        custom_op.parameter_map["mix_compile_mode"].b = False  # 关闭混合计算，根据实际情况配置，默认关闭
        custom_op.parameter_map["precision_mode"].s = tf.compat.as_bytes("allow_mix_precision")  # 设置混合精度
        custom_op.parameter_map["fusion_switch_file"].s = tf.compat.as_bytes("fusion_switch.cfg")
        # # custom_op.parameter_map["enable_data_pre_proc"].b = True  # getnext算子下沉是迭代循环下沉的必要条件
        # # custom_op.parameter_map[
        # #     "iterations_per_loop"].i = 10  # 此处设置的值和set_iteration_per_loop设置的iterations_per_loop值保持一致，用于判断是否进行训练迭代下沉
        # custom_op.parameter_map["dump_path"].s = tf.compat.as_bytes("./dump/")
        # custom_op.parameter_map["enable_dump_debug"].b = True
        # custom_op.parameter_map["dump_debug_mode"].s = tf.compat.as_bytes("all")
        config.graph_options.rewrite_options.remapping = RewriterConfig.OFF  # 必须显式关闭
        config.graph_options.rewrite_options.memory_optimization = RewriterConfig.OFF  # 必须显式关闭
        # sess = tf.train.SingularMonitoredSession(config=config, hooks=hooks, checkpoint_dir=params.output_dir)
        # >>> add code >>


        # config = tf.ConfigProto()
        # config.gpu_options.allow_growth = True
        sess = tf.train.SingularMonitoredSession(config=config, hooks=hooks,
                                                 checkpoint_dir=params.output_dir)
        # reload model
        if params.ckp_path is not "" and FLAGS.reload_model:
            last_checkpoint = tf.train.latest_checkpoint(params.ckp_path)
            print('rolling back to previous checkpoint {0}'.format(last_checkpoint))
            saver.restore(sess, last_checkpoint)

        accum_loss = 0.
        accum_step = 0
        epoch = sess.run(ops['global_step']) // params.num_train_batches
        best_valid_ppl = []
        accum_rate = 0.
        start_time = time.time()
        last_min = (time.time() - start_time) / 60
        cleaned = True
        print('Starting moving_avg')
        sess.run(ops['start_moving_avg'])
        avg_flag = "no_null"
        while True and is_training:
            try:
                loss, gn, lr, should_reset, moving_avg_started, _ = sess.run(run_ops)
                # bptt_rate = sess.run(ops['bptt_rate'])
                # accum_rate += bptt_rate

                accum_loss += loss
                accum_step += 1
                step = sess.run(ops['global_step'])
                if step % params.log_every == 0:
                    # epoch = step // params.num_train_batches
                    train_ppl = np.exp(accum_loss / accum_step)
                    mins_so_far = (time.time() - start_time) / 60.
                    min_pices = mins_so_far-last_min
                    last_min = mins_so_far
                    log_string = 'epoch={0:<5d}'.format(epoch)
                    log_string += ' step={0}/{1:<6d}'.format(step, params.num_train_steps)
                    log_string += ' ppl={0:<10.2f}'.format(train_ppl)
                    log_string += ' lr={0:<6.3f}'.format(lr)
                    log_string += ' |g|={0:<6.3f}'.format(gn)
                    log_string += ' avg={0:<2d}'.format(moving_avg_started)
                    log_string += ' mins={0:<.2f}-min/step={1:<.4f}'.format(mins_so_far, min_pices/params.log_every)
                    # log_string += ' accum_rate(rate of a epoch)={0:<4.6f}'.format(accum_rate)
                    # log_string += ' should_reset:{}'.format(should_reset)
                    print(log_string)

                if moving_avg_started:
                    if avg_flag is "":
                        sess.run(ops['end_moving_avg'])
                        sess.run(ops['reset_avg'])
                        avg_flag = "restart_avg"
                    else:
                        sess.run(ops['update_moving_avg'])
                    # ops['eval_valid'](sess, use_moving_avg=moving_avg_started)


                if step <= (300 * params.num_train_batches):
                    if step % (10 * params.num_train_batches) == 0:
                        print('Start learning decay ...')
                        sess.run(ops['set_lr_decay'])
                if moving_avg_started and step + 5 % (10 * params.num_train_batches) == 0 and len(best_valid_ppl) > params.best_valid_ppl_threshold and valid_ppl > min(best_valid_ppl[:-params.best_valid_ppl_threshold]):
                    print('Start learning decay ...')
                    sess.run(ops['set_lr_decay'])
                if should_reset:
                    accum_rate=0.
                    print("should_reset:{}".format(should_reset))
                    sess.run(ops['reset_batch_states'])
                    epoch += 1
                    accum_loss = 0
                    accum_step = 0
                    valid_ppl = ops['eval_valid'](sess, use_moving_avg=moving_avg_started)
                    # 初始化验证集idx
                    sess.run(ops['reset_start_idx_eval'])
                    # 初始化训练集 batch_state, idx
                    sess.run([ops['reset_batch_states'], ops['reset_start_idx']])
                    # note:当目前的ppl不是最好的10个时，利用移动平均权重法进行调整。
                    if (not moving_avg_started and
                            len(best_valid_ppl) > params.best_valid_ppl_threshold and
                            valid_ppl > min(best_valid_ppl[:-params.best_valid_ppl_threshold])
                    ):
                        print('Starting moving_avg')
                        sess.run(ops['start_moving_avg'])
                    #     print('Start learning decay ...')
                    #     sess.run(ops['set_lr_decay'])

                    if valid_ppl > 15.:
                        best_valid_ppl.append(valid_ppl)
                    if not cleaned:
                        best_valid_ppl = [p for p in best_valid_ppl if p < 40.]
                        cleaned = True
                    # ops['eval_test'](sess, use_moving_avg=moving_avg_started)
                if step % (1 * params.num_train_batches) == 0:
                    test_ppl = ops['eval_test'](sess, use_moving_avg=moving_avg_started)
                    print("test_ppl:{}".format(test_ppl))
                    sess.run(ops['reset_start_idx_eval'])
                if step >= params.num_train_steps:
                    #inference
                    test_ppl =  ops['eval_test'](sess, use_moving_avg=moving_avg_started)
                    print("final_test_ppl:{}".format(test_ppl))
                    break
            except tf.errors.InvalidArgumentError:
                last_checkpoint = tf.train.latest_checkpoint(params.output_dir)
                print('rolling back to previous checkpoint {0}'.format(last_checkpoint))
                saver.restore(sess, last_checkpoint)
                accum_loss, accum_step = 0., 0
        if not is_training:
            moving_avg_started = sess.run(ops['moving_avg_started'])
            test_ppl = ops['eval_test'](sess, use_moving_avg=moving_avg_started)
            sess.close()
            # infer_loss = ops['inference']()
            with tf.Session() as sess:
                print("test_ppl:{}".format(test_ppl))
                #保存图，在./pb_model文件夹中生成model.pb文件
                # model.pb文件将作为input_graph给到接下来的freeze_graph函数
                tf.train.write_graph(sess.graph_def, './models_pb', 'model3.pb')    # 通过write_graph生成模型文件
                freeze_graph.freeze_graph(
                    input_graph='./models_pb/model3.pb',   # 传入write_graph生成的模型文件
                    input_saver='',
                    input_binary=False,
                    input_checkpoint=params.ckp_path+'model.ckpt-906',  # 传入训练生成的checkpoint文件
                    output_node_names='output',  # 与定义的推理网络输出节点保持一致
                    restore_op_name='save/restore_all',
                    filename_tensor_name='save/Const:0',
                    output_graph='./models_pb/enas_lm3.pb',   # 改为需要生成的推理网络的名称
                    clear_devices=False,
                    initializer_nodes='')
                print("done pb!")
        else:
            sess.close()
        """
        if not is_training:
            return infer_loss
        else:
            return -1
        """

def main(unused_args):
    tf.logging.set_verbosity(tf.logging.INFO)
    tf.logging.info("**********")
    print("===>>>data_path:{}".format(FLAGS.data_path))
    print("===>>>output_dir:{}".format(FLAGS.output_dir))
    print("===>>>ckp_path:{}".format(FLAGS.ckp_path))

    print('-' * 80)
    output_dir = FLAGS.output_dir

    print('-' * 80)
    if not gfile.IsDirectory(output_dir):
        print('Path {} does not exist. Creating'.format(output_dir))
        gfile.MakeDirs(output_dir)
    elif FLAGS.reset_output_dir:
        print('Path {} exists. Reseting'.format(output_dir))
        gfile.DeleteRecursively(output_dir)
        gfile.MakeDirs(output_dir)

    print('-' * 80)
    log_file = os.path.join(output_dir, 'stdout')
    print('Logging to {}'.format(log_file))
    sys.stdout = utils.Logger(log_file)

    params = contrib_training.HParams(
        data_path=FLAGS.data_path,
        log_every=FLAGS.log_every,
        output_dir=FLAGS.output_dir,
        ckp_path=FLAGS.ckp_path,
    )

    train(params, is_training=FLAGS.is_training)


if __name__ == '__main__':
    tf.app.run()
