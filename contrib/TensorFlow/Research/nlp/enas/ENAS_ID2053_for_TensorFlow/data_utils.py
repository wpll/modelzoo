# coding=utf-8
# Copyright 2021 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Load picked Penn Treebank data."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# from npu_bridge.npu_init import *

import numpy as np
import tensorflow.compat.v1 as tf


def input_producer(raw_data, batch_size, num_steps, shuffle=False,
                   randomize=False, random_len=False):
    """Produces graph-based input for Penn Treebank.

    Args:
      raw_data: np tensor of size [num_words].
      batch_size: self-explained.
      num_steps: number of BPTT steps.
      shuffle: whether to shuffle sentences.
      randomize: use random segments instead of the continuous corpus.
      random_len: random sequence len.

    Returns:
      If `random_len` is set, return op that represents whether we have reached
        the end of a sequence.
      Otherwise, return number of batches in an epoch.
    """
    print("raw_data_size:{}".format(np.size(raw_data)))
    print("num_steps:{}".format(num_steps))
    batch_len = np.size(raw_data) // batch_size
    num_batches_per_epoch = ((np.size(raw_data) // batch_size) - 1) // num_steps
    print("num_batches_per_epoch:{}".format(num_batches_per_epoch))
    raw_data = tf.convert_to_tensor(raw_data, name='raw_data', dtype=tf.int32)

    # data_len = tf.size(raw_data)


    print("batch_len:{}".format(batch_len))
    data = tf.reshape(raw_data[0: batch_size * batch_len],
                      [batch_size, batch_len])

    epoch_size = (batch_len - 1) // num_steps
    with tf.device('/cpu:0'):
        epoch_size = tf.identity(epoch_size, name='epoch_size')

        if random_len:
            start_idx = tf.Variable(0, name='start_idx', dtype=tf.int32,trainable=False)
            # start_idx = 0
            base_bptt = tf.cond(
                tf.random_uniform(shape=(), minval=0., maxval=1.) < 0.95,
                lambda: tf.cast(num_steps, dtype=tf.float32),
                lambda: tf.cast(num_steps, dtype=tf.float32) / 2.)
            # base_bptt = int(tf.cond(
            #     tf.greater_equal(0.95, np.random.uniform(100)/100),
            #     lambda:num_steps / 1.,
            #     lambda:num_steps / 2.).item())
            # base_bptt = 35
            seq_len = tf.random.truncated_normal(shape=(), mean=base_bptt, stddev=5.,
                                             dtype=tf.float32)
            # seq_len = int(np.random.normal(num_steps, 5))
            # seq_len = 35
            seq_len = tf.cast(seq_len, dtype=tf.int32)
            seq_len = tf.minimum(seq_len, num_steps + 20)  # seq_len <= bptt + 40
            seq_len = tf.minimum(seq_len, batch_len - start_idx - 1)

            # seq_len = tf.cond(tf.greater_equal(seq_len, num_steps + 20), lambda: num_steps + 20, lambda: seq_len).item()
            # seq_len = tf.cond(tf.greater_equal(seq_len, int(batch_len - start_idx - 1)), lambda: int(batch_len - start_idx - 1), lambda: seq_len).item()
            # seq_len = min(seq_len, num_steps + 20, batch_len - start_idx - 1)
            print("seq_len:{}, type:{}".format(seq_len, type(seq_len)))

            end_idx = start_idx + seq_len

            x = data[:, start_idx: end_idx]
            # x = tf.reshape(x, [batch_size, seq_len])
            # print("xshape:{}".format(x.get_shape().as_list()))
            y = data[:, start_idx + 1: end_idx + 1]
            # y = tf.reshape(y, [batch_size, seq_len])
            # print("yshape:{}".format(y.get_shape().as_list()))

            with tf.control_dependencies([x, y]):
                with tf.control_dependencies([tf.assign(start_idx, end_idx)]):
                    should_reset = tf.greater_equal(end_idx, batch_len - 3)
            reset_start_idx = tf.assign(start_idx, 0)
            # reset_start_idx = tf.assign(tf.Variable(start_idx, name='reset_start_idx', dtype=tf.int32, trainable=False), 0)
            return (x, y, num_batches_per_epoch, reset_start_idx, should_reset,
                    base_bptt, seq_len / batch_len)

        if randomize:
            i = tf.random_uniform([1], minval=0, maxval=batch_len - num_steps,dtype=tf.int32)[0]
            x = tf.strided_slice(data, [0, i], [batch_size, i + num_steps])
            y = tf.strided_slice(data, [0, i + 1], [batch_size, i + num_steps + 1])
        else:
            # """
            # 修改点
            start_idx_eval = tf.Variable(0, name='start_idx', dtype=tf.int32,
                             trainable=False)
            seq_len = num_steps
            seq_len = tf.cast(seq_len, dtype=tf.int32)
            end_idx = start_idx_eval + seq_len
            x = data[:, start_idx_eval: end_idx]
            y = data[:, start_idx_eval + 1: end_idx + 1]
            with tf.control_dependencies([x, y]):
                with tf.control_dependencies([tf.assign(start_idx_eval, end_idx)]):
                    should_reset_eval = tf.greater_equal(end_idx, batch_len - num_steps - 3)
            reset_start_idx_eval = tf.assign(start_idx_eval, 0)
        x.set_shape([batch_size, num_steps])
        y.set_shape([batch_size, num_steps])

        return x, y, num_batches_per_epoch, reset_start_idx_eval, should_reset_eval
