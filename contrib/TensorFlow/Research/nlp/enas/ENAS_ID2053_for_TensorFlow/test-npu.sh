#!/bin/bash
### Do not need to Configure CANN Environment on Modelarts Platform, because it has been set already.
### Modelarts Platform command for train

#export ASCEND_GLOBAL_LOG_LEVEL=4      # 日志级别设置  debug级别为0;info 级别为1;warning级别为 2;error级别为3;null级别为4
#export ASCEND_SLOG_PRINT_TO_STDOUT=1  # plog日志是否打屏
#export ASCEND_HOST_LOG_FILE_NUM=1000
#export ASCEND_LOG_DEVICE_FLUSH_TIMEOUT=0
#export ASCEND_GLOBAL_EVENT_ENABLE=0   # 设置事件级别  不开启Event日志级别为0；开启Event日志级别为1
#export ASCEND_GLOBAL_TRACE_ENABLE=0
#export PROFILING_MODE=false
#export PROFILING_OPTIONS='{"output":"/tmp/profiling","training_trace":"off","task_trace":"off","aicpu":"on","fp_point":"resnet_model/conv2d/Conv2Dresnet_model/batch_normalization/FusedBatchNormV3_Reduce","bp_point":"gradients/AddN_70","aic_metrics":"PipeUtilization"}'

export TF_CPP_MIN_LOG_LEVEL=2                   ## Tensorflow api print Log Config
#export ENABLE_FORCE_V2_CONTROL=1

code_dir=${1}
data_path=${2}
output_dir=${3}
ckp_path=${4}

current_time=`date "+%Y-%m-%d-%H-%M-%S"`
FIXED_ARC='0 2 1 0 2 0 3 0 4 2 5 3 5 0 6 0 7 0'

nohup python3 ${code_dir}/fixed.py \
        --data_path=${data_path}/ptb.pkl \
        --output_dir=${output_dir} \
        --fixed_arc='0 2 1 0 2 0 3 0 4 2 5 3 5 0 6 0 7 0' \
        --ckp_path=${ckp_path} \
        --platform='modelarts' \
 	> nohup1.out 2>&1 &


#FIXED_ARC='0 2 1 0 2 1 2 2 4 0 5 0 3 2 6 2'
#
#BASE_PATH = '/home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow'
#
#OUTPUT_DIR=$BASE_PATH'/enas_lm_npu_20211114162907/src/output/test'
#
#DATA_PATH='/home/ma-user/modelarts/inputs/data_url_0/ptb/ptb.pkl'
#
#args ='--fixed_arc=FIXED_ARC --output_dir=$OUTPUT_DIR --data_path=$DATA_PATH'
#
##run test
#python3 /home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/fixed.py $args
