from npu_bridge.npu_init import *
import os
import sys, getopt


def main(argv):
    # print(argv)
    # argv_ = ['-t', 'search']
    runType = ""
    try:
        opts, args = getopt.getopt(argv, "ht:", ["trun="])
    except getopt.GetoptError:
        print("getopt.GetoptError!!")
        print("useage: (sudo) python(3) pythonFileName.py -t <runType>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("useage: pythonFileName.py -t <runType>")
            sys.exit()
        elif opt in ("-t", "--trun"):
            runType = arg
    if runType == "search":
        print(f'runType={runType}!\n')
        os.system("python /home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/search.py --output_dir=/home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/output/search --data_path=/home/ma-user/modelarts/inputs/data_url_0/ptb/ptb.pkl")
    elif runType == "test-npu":
        print(f'runType={runType}!\n')
        os.system("python /home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/fixed.py --fixed_arc='0 2 1 0 2 1 2 2 4 0 5 0 3 2 6 2' --output_dir=/home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/output/test --data_path=/home/ma-user/modelarts/inputs/data_url_0/ptb/ptb.pkl")
        # os.system("python /home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/fixed.py --fixed_arc = '0 2 1 0 2 1 2 2 4 0 5 0 3 2 6 2' --output_dir=$(pwd)/output/test --data_path=$(pwd)/ptb/ptb.pkl")
        # print("this part is writing...")
        # pass
    else:
        print("This runType is invaild!!!")


if __name__ == "__main__":
    main(sys.argv[1:])
