#!/bin/bash
### Do not need to Configure CANN Environment on Modelarts Platform, because it has been set already.
### Modelarts Platform command for train

#export ASCEND_GLOBAL_LOG_LEVEL=1      # 日志级别设置  debug级别为0;info 级别为1;warning级别为 2;error级别为4
export ASCEND_SLOG_PRINT_TO_STDOUT=0  # plog日志是否打屏
#export ASCEND_GLOBAL_EVENT_ENABLE=0   # 设置事件级别  不开启Event日志级别为0；开启Event日志级别为1

export TF_CPP_MIN_LOG_LEVEL=2                   ## Tensorflow api print Log Config
#export ENABLE_FORCE_V2_CONTROL=1

code_dir=${1}
data_path=${2}
output_dir=${3}
obs_url=${4}

current_time=`date "+%Y-%m-%d-%H-%M-%S"`

python ${code_dir}/search.py \
        --data_path=${data_path}/ptb.pkl \
        --output_dir=${output_dir} \
        --obs_dir=${obs_url} \
        --platform='modelarts' \
        2>&1 | tee ${output_dir}/${current_time}_train_npu.log


#BASE_PATH='/home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow'
#
#OUTPUT_DIR=$BASE_PATH'/enas_lm_npu_20211114162907/src/output/search'
#
#DATA_PATH='/home/ma-user/modelarts/inputs/data_url_0/ptb/ptb.pkl'
#
#args="--output_dir=$OUTPUT_DIR --data_path=$DATA_PATH"
#
##run search
#python /home/ma-user/modelarts/user-job-dir/enas_lm_npu_for_TensorFlow/enas_lm_npu_20211114162907/src/search.py $args
