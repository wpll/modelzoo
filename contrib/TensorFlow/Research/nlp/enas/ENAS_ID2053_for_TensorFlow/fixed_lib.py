# coding=utf-8
# Copyright 2021 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""AWD ENAS fixed model."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from npu_bridge.npu_init import *
from npu_bridge.estimator.npu import npu_convert_dropout

import numpy as np
import tensorflow.compat.v1 as tf

import data_utils
import utils

flags = tf.app.flags
FLAGS = flags.FLAGS

flags.DEFINE_string('fixed_arc', '0 2 1 0 2 0 3 0 4 2 5 3 5 0 6 0 7 0', '')
flags.DEFINE_float('child_alpha', 0.7, 'activation L2 reg')
flags.DEFINE_float('child_drop_e', 0.125, 'drop rate words')
flags.DEFINE_float('child_drop_i', 0.175, 'drop rate embeddings')
flags.DEFINE_float('child_drop_l', 0.225, 'drop rate between layers')
flags.DEFINE_float('child_drop_o', 0.75, 'drop rate output')
flags.DEFINE_float('child_drop_w', 0.00, 'drop rate weight')
flags.DEFINE_float('child_drop_x', 0.725, 'drop rate at input of RNN cells')
flags.DEFINE_float('child_init_range', 0.05, '')
flags.DEFINE_float('child_grad_bound', 0.25, '')
flags.DEFINE_float('child_weight_decay', 2e-6, '')
flags.DEFINE_integer('child_num_train_epochs', 2, '')
flags.DEFINE_integer('child_hidden_size', 800, '')


def _gen_mask(shape, drop_prob):
    """Generate a droppout mask."""
    keep_prob = 1. - drop_prob
    mask = tf.random_uniform(shape, minval=0., maxval=1., dtype=tf.float32)
    mask = tf.floor(mask + keep_prob) / keep_prob
    return mask


def _rnn_fn(x, prev_s, w_prev, w_skip, input_mask, layer_mask, params):
    """Multi-layer LSTM.

    Args:
      x: [batch_size, num_steps, hidden_size].
      prev_s: [batch_size, hidden_size].
      w_prev: [2 * hidden_size, 2 * hidden_size].
      w_skip: [None, [hidden_size, 2 * hidden_size] * (num_layers-1)].
      input_mask: [batch_size, hidden_size].
      layer_mask: [batch_size, hidden_size].
      params: hyper-params object.

    Returns:
      next_s: [batch_size, hidden_size].
      all_s: [[batch_size, num_steps, hidden_size] * num_layers].
    """
    batch_size = x.get_shape()[0].value
    print("batch_size:{}".format(batch_size))
    # batch_size = params.batch_size
    num_steps = tf.shape(x)[1]
    fixed_arc = params.fixed_arc
    num_layers = len(fixed_arc) // 2
    set_shape = x.get_shape().as_list()
    print("x.set_shape:{}".format(set_shape))

    # all_s = tf.TensorArray(dtype=tf.float32, size=num_steps, infer_shape=False)
    # all_s_my = []
    all_s_my = tf.zeros([1, batch_size, params.hidden_size], dtype=tf.float32)


    def _condition(step, *unused_args):
        return tf.less(step, num_steps)

    def _body(step, prev_s, all_s_my):
        """Body fn for `tf.while_loop`."""
        inp = x[:, step, :]
        # print("inp:{}".format(inp))
        if layer_mask is not None:
            assert input_mask is not None
            ht = tf.matmul(
                tf.concat([inp * input_mask, prev_s * layer_mask], axis=1), w_prev)
        else:
            ht = tf.matmul(tf.concat([inp, prev_s], axis=1), w_prev)
        # print("w_prev:{}".format(w_prev))
        h, t = tf.split(ht, 2, axis=1)
        h = tf.tanh(h)
        t = tf.sigmoid(t)
        s = prev_s + t * (h - prev_s)
        layers = [s]

        def _select_function(h, function_id):
            if function_id == 0:
                return tf.tanh(h)
            elif function_id == 1:
                return tf.nn.relu(h)
            elif function_id == 2:
                return tf.sigmoid(h)
            elif function_id == 3:
                return h
            raise ValueError('Unknown func_idx {0}'.format(function_id))

        start_idx = 0
        for layer_id in range(num_layers):
            prev_idx = fixed_arc[start_idx]
            func_idx = fixed_arc[start_idx + 1]
            prev_s = layers[prev_idx]
            if layer_mask is not None:
                ht = tf.matmul(prev_s * layer_mask, w_skip[layer_id])
            else:
                ht = tf.matmul(prev_s, w_skip[layer_id])
            h, t = tf.split(ht, 2, axis=1)

            h = _select_function(h, func_idx)
            t = tf.sigmoid(t)
            s = prev_s + t * (h - prev_s)
            # print("layers_id:{}\ns before set_shape:{}".format(layer_id, s))
            s.set_shape([batch_size, params.hidden_size])
            # print("s after set_shape:{}".format(s))
            layers.append(s)
            start_idx += 2
        # print("layers:{}\ns:{}".format(layers, s))
        next_s = tf.add_n(layers[1:]) / tf.cast(num_layers, dtype=tf.float32)
        # print("next_s:{}".format(next_s))
        t = tf.stack([next_s])
        # print("t:{}".format(t))
        all_s_my = tf.concat([all_s_my, t], 0)
        # print("all_s_my:{}".format(all_s_my))
        # all_s.append(next_s)
        return step + 1, next_s, all_s_my

    loop_inps = [tf.constant(0, dtype=tf.int32), prev_s, all_s_my]
    _, next_s, all_s_my = tf.while_loop(_condition, _body, loop_inps, shape_invariants=[loop_inps[0].get_shape(), loop_inps[1].get_shape(), tf.TensorShape([None, batch_size, params.hidden_size])])
    # >>> add code >>>
    # all_s_my = tf.reshape(all_s_my, [set_shape[1]+1, set_shape[0], params.hidden_size])
    # print("all_s_my(list):{}".format(all_s_my))
    # tmp = all_s_my[1:, :, :]
    # # tmp = tf.reshape(tmp, [set_shape[1], set_shape[0], params.hidden_size])
    # print("stack_all_s:{}".format(tmp))
    # all_s = tf.transpose(tmp, perm=[1, 0, 2])
    # # all_s.set_shape([set_shape[0], set_shape[1], params.hidden_size])
    # all_s = tf.reshape(all_s, [set_shape[0], set_shape[1], params.hidden_size])
    # print("all_s:{}".format(all_s))
    all_s_my = tf.strided_slice(all_s_my, [1, 0, 0], [num_steps + 1, batch_size, params.hidden_size])
    # print("stack_all_s:{}".format(all_s_my))

    all_s = tf.transpose(all_s_my, perm=[1, 0, 2])
    # print("all_s:{}".format(all_s))

    return next_s, all_s


def _set_default_params(params):
    """Set default values for the hparams."""
    params.add_hparam('alpha', FLAGS.child_alpha)  # activation L2 reg
    params.add_hparam('best_valid_ppl_threshold', 10)

    params.add_hparam('batch_size', 64)
    params.add_hparam('bptt_steps', 32)

    # for dropouts: dropping rate, NOT keeping rate
    params.add_hparam('drop_e', FLAGS.child_drop_e)  # word
    params.add_hparam('drop_i', FLAGS.child_drop_i)  # embeddings
    params.add_hparam('drop_l', FLAGS.child_drop_l)  # between RNN nodes
    params.add_hparam('drop_o', FLAGS.child_drop_o)  # output
    params.add_hparam('drop_w', FLAGS.child_drop_w)  # weight
    params.add_hparam('drop_x', FLAGS.child_drop_x)  # input to RNN layers

    assert FLAGS.fixed_arc is not None
    print(FLAGS.fixed_arc)
    L_arc = FLAGS.fixed_arc.split(' ')
    print("L_arc:{}".format(L_arc))
    params.add_hparam('fixed_arc', [int(d) for d in L_arc])

    params.add_hparam('grad_bound', FLAGS.child_grad_bound)
    params.add_hparam('hidden_size', FLAGS.child_hidden_size)
    params.add_hparam('init_range', FLAGS.child_init_range)
    params.add_hparam('learning_rate', 40.)
    params.add_hparam('num_train_epochs', FLAGS.child_num_train_epochs)
    params.add_hparam('num_warmup_epochs', 0.0)
    params.add_hparam('vocab_size', 10000)

    params.add_hparam('weight_decay', FLAGS.child_weight_decay)
    return params


class LM(object):
    """Language model."""

    def __init__(self, params, x_train, x_valid, x_test, name='language_model'):
        print('-' * 80)
        print('Building LM')

        self.params = _set_default_params(params)
        self.name = name

        # train data
        (self.x_train, self.y_train,
         self.num_train_batches, self.reset_start_idx,
         self.should_reset,
         self.base_bptt, self.bptt_rate) = data_utils.input_producer(
            x_train, params.batch_size, params.bptt_steps, random_len=True)
        params.add_hparam(
            'num_train_steps', self.num_train_batches * params.num_train_epochs)

        # valid data
        (self.x_valid, self.y_valid,
         self.num_valid_batches, self.reset_start_idx_eval, self.should_reset_eval) = data_utils.input_producer(
            x_valid, params.batch_size, params.bptt_steps)

        # test data
        (self.x_test, self.y_test,
         self.num_test_batches, self.reset_start_idx_eval, self.should_reset_eval) = data_utils.input_producer(x_test, 1, 1)

        params.add_hparam('num_warmup_steps',
                          params.num_warmup_epochs * self.num_train_batches)
        self._build_params()
        self._build_train()
        self._build_valid()
        self._build_test()
        self._build_infer()
        self._build_avg_infer()

    def _build_params(self):
        """Create model parameters."""

        print('-' * 80)
        print('Building model params')
        initializer = tf.initializers.random_uniform(minval=-self.params.init_range,
                                                     maxval=self.params.init_range)
        with tf.variable_scope(self.name, initializer=initializer):
            with tf.variable_scope('embedding'):
                w_emb = tf.get_variable(
                    'w', [self.params.vocab_size, self.params.hidden_size],
                    initializer=initializer)
                # >>> add code >>>
                dropped_w_emb = npu_ops.dropout(w_emb, 1 - self.params.drop_e)
                # >>> add code >>>
                # dropped_w_emb = tf.layers.dropout(
                #     w_emb, self.params.drop_e, [self.params.vocab_size, 1],
                #     training=True)

            hidden_size = self.params.hidden_size
            fixed_arc = self.params.fixed_arc
            num_layers = len(fixed_arc) // 2
            with tf.variable_scope('rnn_cell'):
                w_prev = tf.get_variable('w_prev', [2 * hidden_size, 2 * hidden_size])
                i_mask = tf.ones([hidden_size, 2 * hidden_size], dtype=tf.float32)
                h_mask = _gen_mask([hidden_size, 2 * hidden_size], self.params.drop_w)
                mask = tf.concat([i_mask, h_mask], axis=0)
                dropped_w_prev = w_prev * mask

                w_skip, dropped_w_skip = [], []
                for layer_id in range(num_layers):
                    mask = _gen_mask([hidden_size, 2 * hidden_size], self.params.drop_w)
                    with tf.variable_scope('layer_{}'.format(layer_id)):
                        w = tf.get_variable('w', [hidden_size, 2 * hidden_size])
                        dropped_w = w * mask
                        w_skip.append(w)
                        dropped_w_skip.append(dropped_w)

            with tf.variable_scope('init_states'):
                with tf.variable_scope('batch'):
                    init_shape = [self.params.batch_size, hidden_size]
                    batch_prev_s = tf.get_variable(
                        's', init_shape, dtype=tf.float32, trainable=False)
                    zeros = np.zeros(init_shape, dtype=np.float32)
                    batch_reset = tf.assign(batch_prev_s, zeros)
                with tf.variable_scope('test'):
                    init_shape = [1, hidden_size]
                    test_prev_s = tf.get_variable(
                        's', init_shape, dtype=tf.float32, trainable=False)
                    zeros = tf.zeros(init_shape, dtype=tf.float32)
                    test_reset = tf.assign(test_prev_s, zeros)

        num_params = sum([np.prod(v.shape) for v in tf.trainable_variables()])
        print('Model has {0} params'.format(num_params))

        self.batch_init_states = {
            's': batch_prev_s,
            'reset': batch_reset,
        }
        self.train_params = {
            'w_emb': dropped_w_emb,
            'w_prev': dropped_w_prev,
            'w_skip': dropped_w_skip,
            'w_soft': w_emb,
        }
        self.test_init_states = {
            's': test_prev_s,
            'reset': test_reset,
        }
        self.eval_params = {
            'w_emb': w_emb,
            'w_prev': w_prev,
            'w_skip': w_skip,
            'w_soft': w_emb,
        }

    def _forward(self, x, y, model_params, init_states, is_training=False):
        """Computes the logits.

        Args:
          x: [batch_size, num_steps], input batch.
          y: [batch_size, num_steps], output batch.
          model_params: a `dict` of params to use.
          init_states: a `dict` of params to use.
          is_training: if `True`, will apply regularizations.

        Returns:
          loss: scalar, cross-entropy loss
        """
        w_emb = model_params['w_emb']
        w_prev = model_params['w_prev']
        w_skip = model_params['w_skip']
        w_soft = model_params['w_soft']
        prev_s = init_states['s']

        emb = tf.nn.embedding_lookup(w_emb, x)
        batch_size = self.params.batch_size
        hidden_size = self.params.hidden_size
        if is_training:
            # >>> add code >>>
            emb = npu_ops.dropout(emb, 1-self.params.drop_i) # , [batch_size, 1, hidden_size]) # , training=True)

            # >>> add code >>>
            # emb = tf.layers.dropout(
            #     emb, self.params.drop_i,
            #     [self.params.batch_size, 1, hidden_size], training=True)

            input_mask = _gen_mask([batch_size, hidden_size], self.params.drop_x)
            layer_mask = _gen_mask([batch_size, hidden_size], self.params.drop_l)
        else:
            input_mask = None
            layer_mask = None

        out_s, all_s = _rnn_fn(emb, prev_s, w_prev, w_skip, input_mask, layer_mask,
                               self.params)
        top_s = all_s
        if is_training:
            # >>> add code >>>
            top_s = npu_ops.dropout(top_s,
                                    1 - self.params.drop_o)# ,[self.params.batch_size, 1, self.params.hidden_size]) # , training=True)
            # >>> add code >>>

            # top_s = tf.layers.dropout(top_s, self.params.drop_o,
            #                           [batch_size, 1, hidden_size], training=True)

        carry_on = [tf.assign(prev_s, out_s)]
        # print("top_s:{}\nw_soft:{}".format(top_s, w_soft))
        logits = tf.einsum('bnh,vh->bnv', top_s, w_soft)
        # print("logits:{}".format(logits))
        loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y,
                                                              logits=logits)
        loss = tf.reduce_mean(loss)

        reg_loss = loss  # loss + regularization_terms, for training only
        # print("_forward/loss:{}".format(loss))
        if is_training:
            # L2 weight reg
            reg_loss += self.params.weight_decay * tf.add_n(
                [tf.reduce_sum(tf.cast(w, tf.float32) ** 2) for w in tf.trainable_variables()])

            # activation L2 reg
            reg_loss += self.params.alpha * tf.reduce_mean(all_s ** 2)

        with tf.control_dependencies(carry_on):
            loss = tf.identity(loss)
            if is_training:
                reg_loss = tf.identity(reg_loss)
        # print("reg_loss:{}\nloss:{}".format(reg_loss, loss))
        return reg_loss, loss

    def _build_train(self):
        """Build training ops."""
        print('-' * 80)
        print('Building train graph')
        reg_loss, loss = self._forward(self.x_train, self.y_train,
                                       self.train_params, self.batch_init_states,
                                       is_training=True)

        tf_vars = tf.trainable_variables()
        # print("reg_loss:{}".format(reg_loss))
        print("tf_vars:{}".format(tf_vars))
        global_step = tf.train.get_or_create_global_step()
        lr_scale = (tf.cast(tf.shape(self.y_train)[-1], dtype=tf.float32) /
                    tf.cast(self.params.bptt_steps, dtype=tf.float32))
        with tf.variable_scope('HParam'):
            lr_decay = tf.get_variable('learning_rate_decay', [], initializer=tf.constant_initializer(1.), dtype=tf.float32, trainable=False)
        self.set_lr_decay = tf.assign_sub(lr_decay, 0.02*lr_decay)
        learning_rate = utils.get_lr(global_step, self.params, lr_decay) * lr_scale
        grads = tf.gradients(reg_loss, tf_vars)
        # print("grads:{}".format(grads))
        clipped_grads, grad_norm = tf.clip_by_global_norm(grads,
                                                          self.params.grad_bound)
        (self.update_moving_avg_ops, self.use_moving_avg_vars,
         self.restore_normal_vars) = self._create_average_ops()
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)
        train_op = optimizer.apply_gradients(zip(clipped_grads, tf_vars),
                                             global_step=global_step)

        self.train_loss = loss
        self.train_op = train_op
        self.grad_norm = grad_norm
        self.learning_rate = learning_rate

    # def _EMA(self):
    #     """Build moving average ops."""
    #     print('Creating moving average ops')
    #
    #     with tf.variable_scope('moving_avg_flag'):
    #         self.moving_avg_started = tf.get_variable(
    #             'flag', [], tf.int32, initializer=tf.initializers.zeros(),
    #             trainable=False)
    #         self.start_moving_avg_op = tf.assign(self.moving_avg_started, 1)
    #         self.end_moving_avg_op = tf.assign(self.moving_avg_started, 0)
    #     all_vars = tf.trainable_variables()
    #
    #     ema = tf.train.ExponentialMovingAverage(0.99)
    #
    #     average_op = ema.apply(all_vars)
    #     back_up_v = tf.identity(all_vars)
    #     use_average_op = tf.assign(all_vars, ema.average(all_vars))
    #     ema.average_name()
    #     reverse_average_op = tf.assign(all_vars, back_up_v)




    def _create_average_ops(self):
        """Build moving average ops."""
        print('Creating moving average ops')

        with tf.variable_scope('moving_avg_flag'):
            self.moving_avg_started = tf.get_variable(
                'flag', [], tf.int32, initializer=tf.initializers.zeros(),
                trainable=False)
            self.start_moving_avg_op = tf.assign(self.moving_avg_started, 1)
            self.end_moving_avg_op = tf.assign(self.moving_avg_started, 0)

        all_vars = tf.trainable_variables()
        print('all_vars:{}'.format(all_vars))
        average_pairs = []
        var_cnt = 0
        with tf.variable_scope('average'):
            for v in all_vars:
                avg_v = tf.get_variable(
                    str(var_cnt), shape=v.shape, dtype=v.dtype,
                    initializer=tf.zeros_initializer, trainable=False)
                var_cnt += 1
                average_pairs.append([v, avg_v])
        backup_pairs = []
        var_cnt = 0
        with tf.variable_scope('backup'):
            for v in all_vars:
                backup_v = tf.get_variable(str(var_cnt), shape=v.shape, dtype=v.dtype,
                                           trainable=False)
                var_cnt += 1
                backup_pairs.append([v, backup_v])
        # 原作者手动实现的Moving Average ::当eval_valid_ppl退化到一定阈值（退步10名）后启动
        with tf.variable_scope('avg_step'):
            avg_step = tf.get_variable('step', [], initializer=tf.constant_initializer(0.), dtype=tf.float32, trainable=False)
        tmp1 = []
        tmp2 = []
        tmp3 = []
        self.restart_avg = tf.assign(avg_step, 0.)
        with tf.control_dependencies([tf.assign_add(avg_step, 1.)]):
            average_op = []
            for v, avg_v in average_pairs:
                # v_curr = tf.Variable(tf.cast(tf.identity(v), tf.float32), dtype=tf.float32, trainable=False)
                # avg_v_curr = tf.Variable(tf.cast(tf.identity(avg_v), tf.float32), dtype=tf.float32, trainable=False)
                # mu = 1. / avg_step
                mu = tf.cond(tf.cast(0.999 < (1. + avg_step) / (10. + avg_step), tf.bool),
                             lambda: tf.cast(tf.constant(0.99), dtype=tf.float32),
                             lambda: tf.cast((1. + avg_step) / (10. + avg_step), dtype=tf.float32))

                new_avg = mu * tf.cast(avg_v, tf.float32) + (1. - mu) * tf.cast(v, tf.float32)
                with tf.control_dependencies([new_avg]):
                    average_op.append(tf.assign(avg_v, tf.cast(new_avg, avg_v.dtype)))
        # 追踪变量
                tmp1.append(v)
                tmp2.append(new_avg)
                tmp3.append([avg_step, mu, tf.reduce_sum(v ** 2), tf.reduce_sum(avg_v ** 2), tf.reduce_sum(new_avg ** 2)])

        self.p1 = tf.add_n([tf.reduce_sum(tf.cast(w, tf.float32) ** 2) for w in tmp1])
        self.p2 = tf.add_n([tf.reduce_sum(tf.cast(w, tf.float32) ** 2) for w in tmp2])
        self.p3 = tmp3
        # # 使用官方API
        # with tf.variable_scope('avg_step'):
        #     avg_step = tf.get_variable('step', [], dtype=tf.float32, trainable=False)
        #
        # ema = tf.train.ExponentialMovingAverage(0.99, avg_step)
        # with tf.control_dependencies([tf.assign_add(avg_step, 1.0)]):
        #     average_op = []
        #     for v, avg_v in average_pairs:
        #         v = tf.Variable(tf.cast(v, tf.float32), dtype=tf.float32, trainable=False)
        #         avg_v = tf.Variable(tf.cast(avg_v, tf.float32), dtype=tf.float32, trainable=False)
        #         print('v:{}'.format(v))
        #         ema.apply([v])
        #         new_avg = ema.average(v)
        #         print('new_avg:{}'.format(new_avg))
        #         with tf.control_dependencies([new_avg]):
        #             print('avg_v:'.format(avg_v))
        #             average_op.append(tf.assign(avg_v, new_avg))
        #     # average_op = tf.group(*average_op)

        assert len(average_pairs) == len(all_vars)
        assert len(average_pairs) == len(backup_pairs)
        use_average_op = []

        new_tmp1 = []
        for i in range(len(average_pairs)):
            v, avg_v = average_pairs[i]
            _, backup_v = backup_pairs[i]
            with tf.control_dependencies([tf.assign(backup_v, v)]):
                new_tmp1.append([tf.reduce_sum(v ** 2), tf.reduce_sum(avg_v ** 2), tf.reduce_sum(backup_v ** 2)])
                use_average_op.append(tf.assign(v, avg_v))
        self.p4 = new_tmp1

        use_average_op = tf.group(*use_average_op)
        # with tf.control_dependencies([use_average_op]):
        self.p3_1 = tf.add_n([tf.reduce_sum(tf.cast(w, tf.float32) ** 2) for w in tf.trainable_variables()])
        reverse_average_op = []
        new_tmp2 = []
        for v, backup_v in backup_pairs:
            # with tf.control_dependencies([use_average_op]):
            new_tmp2.append([tf.reduce_sum(v ** 2), tf.reduce_sum(backup_v ** 2)])
            reverse_average_op.append(tf.assign(v, backup_v))
        self.p5 = new_tmp2
        reverse_average_op = tf.group(*reverse_average_op)
        # with tf.control_dependencies([reverse_average_op]):
        self.p3_2 = tf.add_n([tf.reduce_sum(tf.cast(w, tf.float32) ** 2) for w in tf.trainable_variables()])

        return average_op, use_average_op, reverse_average_op

    def _eval_test(self, sess, use_moving_avg=False):
        """Eval 1 round on test set."""
        total_loss = 0
        if use_moving_avg:
            print('v:{}'.format(tf.trainable_variables()))
            sess.run([self.use_moving_avg_vars, self.test_init_states['reset']])
            print('v_avg:{}'.format(tf.trainable_variables()))
        for step in range(int(self.num_test_batches)):
            total_loss += sess.run(self.test_loss)
            if (step + 1) % 1000 == 0:
                test_ppl = np.exp(total_loss / (step + 1))
                log_string = 'step={0:<6d}'.format(step + 1)
                log_string += ' test_ppl={0:<.2f}'.format(test_ppl)
                print(log_string)
            if sess.run(self.should_reset_eval):
                break
        # test_ppl = np.exp(total_loss / self.num_test_batches)
        
        # log_string = 'step={0:<6d}'.format(self.num_test_batches)
        # log_string += ' test_ppl={0:<.2f}'.format(test_ppl)
        # print(log_string)
        if use_moving_avg:
            sess.run(self.restore_normal_vars)
        # test_ppl = tf.math.exp(total_loss/ self.num_test_batches, name='output')
        # print("test_ppl:{}".format(test_ppl))
     #   loss_assign_op = tf.assign(self.tt_loss, tf.Variable(total_loss, name='total_loss', dtype=tf.float32,trainable=False))

    def _build_valid(self):
        print('Building valid graph')
        _, loss = self._forward(self.x_valid, self.y_valid,
                                self.eval_params, self.batch_init_states)
        self.valid_loss = loss

    def _build_test(self):
        print('Building test graph')
        _, loss = self._forward(self.x_test, self.y_test,
                                self.eval_params, self.test_init_states)
        self.test_loss = loss

    def _build_infer(self):
        print("Building infer graph")
        tt_loss = tf.Variable(0, name="total_loss", dtype=tf.float32, trainable=False)
        def _condition(step, *unused_args):
            return tf.less(step, self.num_test_batches-3)
        def _body(step, tt_loss):
            with tf.control_dependencies([self.test_loss]):
                tt_loss += self.test_loss
            return step+1, tt_loss
        loop_inps = [tf.constant(0, dtype=tf.int32), tt_loss]
        _, tt_loss = tf.while_loop(_condition, _body, loop_inps)
        test_ppl = tf.math.exp(tt_loss/ self.num_test_batches, name='test_ppl')
        print("test_ppl:{}".format(test_ppl))
        self.infer_ppl = test_ppl

    def _build_avg_infer(self):
        print("Build avg_infer graph")
        def _fp():
            with tf.control_dependencies([self.use_moving_avg_vars, self.test_init_states['reset']]):
                avg_infer_ppl = self.infer_ppl
            with tf.control_dependencies([avg_infer_ppl, self.restore_normal_vars]):
                return avg_infer_ppl
        def _fn():
            return self.infer_ppl

        with tf.control_dependencies([self.moving_avg_started]):
            avg_infer_ppl = tf.cond(tf.greater_equal(self.moving_avg_started, 1), _fp, _fn)
        self.avg_infer_ppl = tf.identity(avg_infer_ppl, name="output")
        print("self.avg_infer_ppl:{}".format(self.avg_infer_ppl))


    def eval_valid(self, sess, use_moving_avg=False):
        """Eval 1 round on valid set."""
        total_loss = 0

        if use_moving_avg:
            # print('sum_v:{}'.format(sess.run(self.p1)))
            # print('new_sum_v:{}'.format(sess.run(self.p2)))
            # print('[[step, mu, v, v_avg, new_v_avg]]={}'.format(sess.run(self.p3)))
            # self.use_moving_avg_vars ===>影子权重暂时替代当前权重
            sess.run([self.use_moving_avg_vars, self.batch_init_states['reset']])
            # print('v_avg:{}\n[[v, avg_v, backup_v]]={}'.format(sess.run(self.p3_1), sess.run(self.p4)))

        valid_loss = []
        for _ in range(self.num_valid_batches):
            loss = sess.run(self.valid_loss)
            total_loss += loss
            valid_loss.append(loss)
            if sess.run(self.should_reset_eval):
                break
        print("valid_loss={}, self.num_valid_batches={}".format(valid_loss, self.num_valid_batches))
        valid_ppl = np.exp(total_loss / self.num_valid_batches)
        print('valid_ppl={0:<.2f}'.format(valid_ppl))
        if use_moving_avg:
            sess.run(self.restore_normal_vars)

        # print('v:{}\n[[v, backup_v]]={} \n============================================================'.format(
        #     sess.run(self.p3_2), sess.run(self.p5)))

        return valid_ppl

    def do_infer(self, sess, use_moving_avg=False):
      #  self._eval_test(sess, use_moving_avg)
        return sess.run(self.avg_infer_ppl)
