###基本信息

#### 
发布者（Publisher）：Huawei

应用领域（Application Domain）：NLP

修改时间（Modified） ：2018.

框架（Framework）：TensorFlow 1.15.0

模型格式（Model Format）：ckpt

精度（Precision）：Mixed

处理器（Processor）：昇腾910

应用级别（Categories）：Research

描述（Description）： enas模型用于ptb数据集的神经网络结构搜索

###概述
####
enas是一个快速高效的神经网络架构搜索的方法，使用了子图采样和权重共享的策略,极大的提高了神经网络结构搜索的效率。在数据ptb和cifar-10上都发现了新的网络架构而达到了新的sota.
- 参考论文：[Efficient Neural Architecture Search via Parameter Sharing](http://proceedings.mlr.press/v80/pham18a/pham18a.pdf)
- 参考代码：[enas](https://github.com/melodyguan/enas)
###默认配置
####
- 数据预处理
  - 输入数据为文本
  - 文本输入格式: id [int]
- 训练超参数
  - search
    - #### controller baseline decay : 0.999 
    - #### controller entropy weight : 1e-5 
    - #### controller temperature : 5 
    - #### controller learning rate : 5e-5 
    - #### controller num layers : 9 
    - #### controller hidden size : 64
    - #### controller num functions : 4
    - #### child batch size : 128
    - #### child bptt steps : 35 
    - #### num train epochs : 600
  - ####test
    - #### child grad bound : 0.25 
    - #### child weight decay : 2e-6 
    - #### child num train epochs :3000
    - #### child hidden size : 800 
    - #### learning_rate : 20.

###支持特性

| 特性列表 | 是否支持 |
|------|------|
| 混合精度 | 是    |

###混合精度训练
#### 昇腾910 AI处理器提供自动混合精度功能，可以针对全网中float32数据类型的算子，按照内置的优化策略，自动将部分float32的算子降低精度到float16，从而在精度损失很小的情况下提升系统性能并减少内存使用。

###快速上手
####模型的search阶段和test阶段都使用数据集ptb，原始数据需要使用process.py脚本进行处理，也可以在obs://rstg/Dataset/ptb获取。

###代码结构文件
####
|— search.py 搜索模型代码\
|— child.py  子图模型代码\
|— fixed.py 架构验证模型代码\
|— fixed_lib.py\
|— data_utils.py 数据处理代码\
|— controller.py 性能评估模型代码\
|— boot_modelarts.py 模型运行代码\
|— ...

###脚本参数
#### 
- search:\
--data_path\
--output_dir\
--obs_dir
- test:\
--data_path\
--output_dir\
--fixed_arc\
--ckp_path



###训练过程
在论文的参数设置下，GPU训练精度和速度可以达到要求；
NPU的训练精度和速度还未达标。
- #### GPU
#### search
epoch=0     step=200    /124200    ppl=1950.47   lr=17.14   |w|=0.44   |g|=2.84   mins=0.53\
valid_ppl=1800.73\
epoch=1     step=400    /124200    ppl=1187.87   lr=22.86   |w|=0.64   |g|=0.81   mins=1.46\
valid_ppl=892.87\
epoch=2     step=600    /124200    ppl=1065.44   lr=18.29   |w|=0.82   |g|=0.35   mins=2.36\
valid_ppl=843.70\
epoch=3     step=800    /124200    ppl=953.38    lr=14.86   |w|=1.14   |g\|=0.31   mins=3.25\
valid_ppl=898.45\
epoch=4     step=1000   /124200    ppl=949.04    lr=20.57   |w|=1.72   |g|=0.31   mins=4.15\
valid_ppl=774.25\
epoch=5     step=1200   /124200    ppl=876.15    lr=20.00   |w|=3.69   |g|=0.30   mins=5.04\
valid_ppl=622.82\
epoch=6     step=1400   /124200    ppl=838.09    lr=24.00   |w|=6.94   |g|=0.67   mins=5.92\
valid_ppl=606.77\
epoch=7     step=1600   /124200    ppl=764.65    lr=21.14   |w|=11.46  |g|=0.36   mins=6.81\
valid_ppl=579.69\
epoch=8     step=1800   /124200    ppl=762.31    lr=20.00   |w|=17.41  |g|=0.29   mins=7.71\
valid_ppl=520.63\
epoch=9     step=2000   /124200    ppl=695.99    lr=24.00   |w|=27.42  |g|=0.20   mins=8.61\
...\
valid_ppl=162.39\
epoch=574   step=124200 /124200    ppl=244.80    lr=21.71   |w|=6348.55 |g|=0.15   mins=536.70
#### test
epoch=0     step=200     ppl=1779.60    lr=20.000 |g|=0.234  avg=0  mins=0.34\
epoch=0     step=400     ppl=1223.42    lr=20.571 |g|=0.407  avg=0  mins=0.64\
valid_ppl=463.03\
epoch=1     step=600     ppl=595.22     lr=9.714  |g|=0.483  avg=0  mins=0.98\
epoch=1     step=800     ppl=545.60     lr=24.000 |g|=0.223  avg=0  mins=1.28\
valid_ppl=339.76\
epoch=2     step=1000    ppl=436.82     lr=21.714 |g|=0.332  avg=0  mins=1.61\
epoch=2     step=1200    ppl=411.70     lr=14.286 |g|=0.274  avg=0  mins=1.91\
valid_ppl=271.71\
epoch=3     step=1400    ppl=365.17     lr=18.857 |g|=0.291  avg=0  mins=2.24\
epoch=3     step=1600    ppl=347.84     lr=14.857 |g|=0.247  avg=0  mins=2.54\
valid_ppl=245.00\
epoch=4     step=1800    ppl=321.47     lr=17.143 |g|=0.238  avg=0  mins=2.87\
epoch=4     step=2000    ppl=307.67     lr=18.286 |g|=0.237  avg=0  mins=3.18\
valid_ppl=213.10\
epoch=5     step=2200    ppl=296.59     lr=17.714 |g|=0.259  avg=0  mins=3.51\
epoch=5     step=2400    ppl=281.99     lr=15.429 |g|=0.263  avg=0  mins=3.81\
epoch=6     step=2600    ppl=280.63     lr=22.857 |g|=0.234  avg=0  mins=4.12\
valid_ppl=209.90\
epoch=6     step=2800    ppl=261.67     lr=20.000 |g|=0.232  avg=0  mins=4.44\
epoch=7     step=3000    ppl=262.83     lr=16.000 |g|=0.313  avg=0  mins=4.75\
valid_ppl=181.99\
epoch=7     step=3200    ppl=249.74     lr=8.571  |g|=0.367  avg=0  mins=5.07\
epoch=8     step=3400    ppl=248.14     lr=17.714 |g|=0.248  avg=0  mins=5.37\
valid_ppl=176.79\
epoch=8     step=3600    ppl=243.44     lr=17.714 |g|=0.260  avg=0  mins=5.69\
epoch=9     step=3800    ppl=236.51     lr=17.143 |g|=0.299  avg=0  mins=6.00\
valid_ppl=166.62\
...\
epoch=2997  step=1241000 ppl=51.39      lr=21.714 |g|=0.333  avg=1  mins=2160.67\
epoch=2998  step=1241200 ppl=48.44      lr=21.714 |g|=0.336  avg=1  mins=2161.02\
valid_ppl=61.17\
epoch=2998  step=1241400 ppl=54.42      lr=22.857 |g|=0.322  avg=1  mins=2161.37\
epoch=2999  step=1241600 ppl=48.16      lr=21.714 |g|=0.339  avg=1  mins=2161.70\
epoch=2999  step=1241800 ppl=49.21      lr=21.714 |g|=0.340  avg=1  mins=2162.04\
valid_ppl=61.17\
epoch=3000  step=1242000 ppl=48.24      lr=22.286 |g|=0.332  avg=1  mins=2162.40\
...\
step=70000  test_ppl=59.15\
step=71000  test_ppl=59.03\
step=72000  test_ppl=59.06\
step=73000  test_ppl=58.41\
step=74000  test_ppl=58.24\
step=75000  test_ppl=58.12\
step=76000  test_ppl=58.15\
step=77000  test_ppl=58.29\
step=78000  test_ppl=58.36\
step=79000  test_ppl=58.50\
step=80000  test_ppl=58.43\
step=81000  test_ppl=58.72\
step=82000  test_ppl=58.52\
step=82429  test_ppl=58.64

- #### NPU
#### test
epoch=0     step=200/453000 ppl=6106.61    lr=46.250 |g|=0.171  avg=0  mins=5.33-min/step=0.0211\
epoch=0     step=400/453000 ppl=1966.93    lr=40.000 |g|=0.193  avg=0  mins=9.44-min/step=0.0204\
valid_ppl=389.49\
epoch=1     step=600/453000 ppl=405.67     lr=42.500 |g|=0.195  avg=0  mins=14.69-min/step=0.0208\
epoch=1     step=800/453000 ppl=369.30     lr=38.750 |g|=0.207  avg=0  mins=18.93-min/step=0.0212\
valid_ppl=298.25\
epoch=2     step=1000/453000 ppl=299.71     lr=38.750 |g|=0.222  avg=0  mins=23.45-min/step=0.0243\
epoch=2     step=1200/453000 ppl=281.29     lr=45.000 |g|=0.177  avg=0  mins=27.68-min/step=0.0210\
epoch=2     step=1400/453000 ppl=274.65     lr=43.750 |g|=0.270  avg=0  mins=31.83-min/step=0.0211\
valid_ppl=236.61\
epoch=3     step=1600/453000 ppl=243.76     lr=33.750 |g|=0.209  avg=0  mins=36.26-min/step=0.0208\
epoch=3     step=1800/453000 ppl=240.20     lr=33.750 |g|=0.222  avg=0  mins=40.45-min/step=0.0211\
valid_ppl=252.75\
epoch=4     step=2000/453000 ppl=228.79     lr=40.000 |g|=0.214  avg=0  mins=44.94-min/step=0.0205\
epoch=4     step=2200/453000 ppl=222.90     lr=40.000 |g|=0.211  avg=0  mins=49.15-min/step=0.0210\
valid_ppl=197.03\
epoch=5     step=2400/453000 ppl=219.08     lr=40.000 |g|=0.199  avg=0  mins=53.66-min/step=0.0245\
epoch=5     step=2600/453000 ppl=204.19     lr=32.500 |g|=0.219  avg=0  mins=57.78-min/step=0.0209\
epoch=5     step=2800/453000 ppl=206.65     lr=33.750 |g|=0.225  avg=0  mins=61.98-min/step=0.0210\
valid_ppl=191.64\
epoch=6     step=3000/453000 ppl=197.33     lr=45.000 |g|=0.201  avg=0  mins=66.49-min/step=0.0207\
epoch=6     step=3200/453000 ppl=194.74     lr=38.750 |g|=0.212  avg=0  mins=70.64-min/step=0.0211\
valid_ppl=200.02\
epoch=7     step=3400/453000 ppl=191.74     lr=35.000 |g|=0.208  avg=0  mins=75.13-min/step=0.0240\
epoch=7     step=3600/453000 ppl=186.42     lr=41.250 |g|=0.185  avg=0  mins=79.25-min/step=0.0205\
valid_ppl=201.46\
epoch=8     step=3800/453000 ppl=204.60     lr=46.250 |g|=0.225  avg=0  mins=83.78-min/step=0.0243\
epoch=8     step=4000/453000 ppl=177.41     lr=32.500 |g|=0.236  avg=0  mins=87.95-min/step=0.0208\
epoch=8     step=4200/453000 ppl=180.42     lr=36.250 |g|=0.207  avg=0  mins=92.05-min/step=0.0207\
valid_ppl=175.82\
epoch=9     step=4400/453000 ppl=180.36     lr=35.000 |g|=0.350  avg=0  mins=96.54-min/step=0.0208\
epoch=9     step=4600/453000 ppl=173.57     lr=42.500 |g|=0.188  avg=0  mins=100.67-min/step=0.0206\
valid_ppl=209.94\
epoch=10    step=4800/453000 ppl=170.76     lr=38.750 |g|=0.207  avg=0  mins=105.17-min/step=0.0243\
epoch=10    step=5000/453000 ppl=167.46     lr=32.500 |g|=0.244  avg=0  mins=109.31-min/step=0.0207\
epoch=10    step=5200/453000 ppl=169.23     lr=43.750 |g|=0.235  avg=0  mins=113.42-min/step=0.0203\
valid_ppl=167.50\
...\
valid_ppl=112.40\
epoch=270   step=128000/453000 ppl=98.60      lr=31.389 |g|=0.316  avg=1  mins=2925.00-min/step=0.0222\
epoch=270   step=128200/453000 ppl=95.14      lr=26.773 |g|=0.556  avg=1  mins=2929.33-min/step=0.0211\
valid_ppl=113.40\
epoch=271   step=128400/453000 ppl=98.80      lr=32.312 |g|=0.319  avg=1  mins=2934.05-min/step=0.0257\
epoch=271   step=128600/453000 ppl=92.38      lr=28.620 |g|=0.328  avg=1  mins=2938.40-min/step=0.0214\
epoch=271   step=128800/453000 ppl=96.70      lr=28.620 |g|=0.350  avg=1  mins=2942.81-min/step=0.0218\
valid_ppl=113.22\
epoch=272   step=129000/453000 ppl=96.46      lr=29.543 |g|=0.316  avg=1  mins=2947.51-min/step=0.0218