# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This is the boot file for ModelArts platform.
Firstly, the train datasets are copyed from obs to ModelArts.
Then, the string of train shell command is concated and using 'os.system()' to execute
"""
import os
import time
import numpy as np
import argparse
from help_modelarts import obs_data2modelarts
# import moxing as mox
print(os.system('env'))
print(os.system("python3 --version"))
#print(os.system("pip install dlib"))
print("===>>>hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
os.environ['ASCEND_GLOBAL_LOG_LEVEL'] = '4'

#data_dir = "/root/.keras/models/"
if __name__ == '__main__':
    ## Note: the code dir is not the same as work dir on ModelArts Platform!!!
    code_dir = '.'
    work_dir = os.getcwd()
    print("===>>>code_dir:{}, work_dir:{}".format(code_dir, work_dir))
    output_path = "./output/test/" + str(time.strftime('%Y%m%d_%H%M%S')) 
    parser = argparse.ArgumentParser()
    parser.add_argument("--train_url", type=str, default=output_path)
    parser.add_argument("--data_url", type=str, default="./ptb")
    parser.add_argument("--ckp_path", type=str, default="./output/test/20220715_182127/")
    # parser.add_argument("--ckp_path", type=str, default="obs://rstg/workplace_ENAS/lm-train/MA-new-enas-05-23-19-34/output/result/")
    # parser.add_argument("--modelarts_data_dir", type=str, default="/cache/ptb-dataset")
    # parser.add_argument("--modelarts_result_dir", type=str, default="/cache/result")
    config = parser.parse_args()
    #if not os.path.exists(data_dir):
    #    os.makedirs(data_dir)
    #    print("=nvvvvvvvvvvvvvfdsfdsfdvnn")

    #os.system("pip install -i http://repo.myhuaweicloud.com/repository/pypi/simple pexpect==4.2.1")
    #os.system("pip install torch")
    #os.system("pip install absl-py")
    print("--------config---------hhhhhhhhhhhggggggggggggggggkkkkkkkkkkkkkkkkkkkkkkkkkgg-")
    for k in list(vars(config).keys()):
        print("key:{}: value:{}".format(k, vars(config)[k]))
    print("--------config----------")

    ## copy dataset from obs to modelarts
    # obs_data2modelarts(config)
    # ret = mox.file.exists('obs://rstg/MA-new-p/')
    # retm = mox.file.make_dirs('obs://rstg/MA-new-p/')
    # print("bbbbbbbbbbbbbbbbbbbbbbbbb  ",retm)
    # print("config.modelarts_result_dir  ", config.modelarts_result_dir)
    ## start to train on Modelarts platform
    # if not os.path.exists(config.modelarts_result_dir):
    #     os.makedirs(config.modelarts_result_dir)
    #     print("6666666666666666666666666666666666666666  ", config.modelarts_result_dir)
    bash_header = os.path.join(code_dir, 'test-npu.sh')
    # bash_header = os.path.join(code_dir, 'search.sh')
    arg_url = '%s %s %s %s' % (code_dir, config.data_url, config.train_url, config.ckp_path)
    bash_command = 'bash %s %s' % (bash_header, arg_url)
    print("bash command:", bash_command)
    os.system(bash_command)
